package com.demo.aswitch.touchfeedback;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by Qin_Li_Yang on 2017/11/14.
 * <p>
 * 创建一个TouchFeedback类
 * 这里使用单利模式,应该是可以的哈哈.
 */

public class TouchFeedback {
    /**
     * 缩放动画的单方面时长,假如是100,是指放大100,当手指离开缩小又是100.
     */
    private int duration = 100;
    /**
     * 放大或者缩小的值
     */
    private float aFloatBig = 1.2f;

    /**
     * 可以自己设定
     *
     * @param duration
     * @param aFloatBig
     */
    public void setDurationAndFloatBig(int duration, float aFloatBig) {
        this.duration = duration;
        this.aFloatBig = aFloatBig;
    }

    /**
     * 私有构造
     */
    private TouchFeedback() {
    }

    /**
     * 定义回调的接口
     */
    public interface TouchFeedbackListener {
        void onFeedback(View view);
    }

    private static TouchFeedback touchFeedback = null;

    public static TouchFeedback getTouchFeedbackInstance() {
        if (touchFeedback == null) {
            synchronized (TouchFeedback.class) {
                if (touchFeedback == null) {
                    touchFeedback = new TouchFeedback();
                }
            }
        }
        return touchFeedback;
    }

    /**
     * 动画执行的监听
     * @param touchFeedListener
     * @param view
     */
    public void setTouchFeedListener(final TouchFeedback.TouchFeedbackListener touchFeedListener, View view) {
        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        scaleBig(v);
                        break;
                    case MotionEvent.ACTION_MOVE:

                        break;

                    case MotionEvent.ACTION_UP:
                        scaleSmall(v, true, touchFeedListener);

                        break;
                    case MotionEvent.ACTION_CANCEL:
                        scaleSmall(v, false, touchFeedListener);
                        break;
                }
                return true;
            }
        });
    }

    /**
     * 恢复原形的动画
     * @param v
     * @param b
     * @param touchFeedListener
     */
    private void scaleSmall(final View v, boolean b, final TouchFeedbackListener touchFeedListener) {
        ObjectAnimator objectAnimator2 = ObjectAnimator.ofFloat(v, "ScaleX", aFloatBig, 1f).setDuration(duration);
        ObjectAnimator objectAnimator3 = ObjectAnimator.ofFloat(v, "ScaleY", aFloatBig, 1f).setDuration(duration);
        objectAnimator2.start();
        objectAnimator3.start();
        if (b) {
            objectAnimator2.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                /**
                 * 动画结束
                 * @param animation
                 */
                @Override
                public void onAnimationEnd(Animator animation) {
                    touchFeedListener.onFeedback(v);
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });
        }


    }

    /**
     * 变大的动画
     * @param v
     */
    private void scaleBig(View v) {
        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(v, "ScaleX", 1f, aFloatBig).setDuration(duration);
        ObjectAnimator objectAnimator1 = ObjectAnimator.ofFloat(v, "ScaleY", 1f, aFloatBig).setDuration(duration);
        objectAnimator.start();
        objectAnimator1.start();
    }
}
