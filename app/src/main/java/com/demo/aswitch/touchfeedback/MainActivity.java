package com.demo.aswitch.touchfeedback;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

/**
 * 实现刚才写的那个类里面的TouchFeedbackListener接口
 */
public class MainActivity extends AppCompatActivity implements TouchFeedback.TouchFeedbackListener {

    private Button t1;
    private Button t2;
    private Button t3;
    private Button t4;
    private Button t5;
    private Button t6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }

    private void initView() {
        t1 = (Button) findViewById(R.id.t1);
        t2 = (Button) findViewById(R.id.t2);
        t3 = (Button) findViewById(R.id.t3);
        t4 = (Button) findViewById(R.id.t4);
        t5 = (Button) findViewById(R.id.t5);
        t6 = (Button) findViewById(R.id.t6);

        TouchFeedback touchFeedbackInstance = TouchFeedback.getTouchFeedbackInstance();
        touchFeedbackInstance.setDurationAndFloatBig(100, 1.2f);
        touchFeedbackInstance.setTouchFeedListener(this, t1);
        touchFeedbackInstance.setTouchFeedListener(this, t2);
        touchFeedbackInstance.setTouchFeedListener(this, t3);
        touchFeedbackInstance.setTouchFeedListener(this, t4);
        touchFeedbackInstance.setTouchFeedListener(this, t5);
        touchFeedbackInstance.setTouchFeedListener(this, t6);
    }

    /**
     * 这里像onclick一样
     * @param view
     */
    @Override
    public void onFeedback(View view) {
        switch (view.getId()) {
            case R.id.t1:
                Toast.makeText(this, "t1", Toast.LENGTH_SHORT).show();
                break;
            case R.id.t2:
                Toast.makeText(this, "t2", Toast.LENGTH_SHORT).show();
                break;
            case R.id.t3:
                Toast.makeText(this, "t3", Toast.LENGTH_SHORT).show();
                break;
            case R.id.t4:
                Toast.makeText(this, "t4", Toast.LENGTH_SHORT).show();
                break;
            case R.id.t5:
                Toast.makeText(this, "t5", Toast.LENGTH_SHORT).show();
                break;
            case R.id.t6:
                Toast.makeText(this, "t6", Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
